/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : ins1st-cloud

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 06/12/2019 11:51:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_constants
-- ----------------------------
DROP TABLE IF EXISTS `sys_constants`;
CREATE TABLE `sys_constants` (
  `id` varchar(64) NOT NULL,
  `constants_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量名称',
  `constants_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量key',
  `constants_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量value',
  `constants_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量value',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置';

-- ----------------------------
-- Records of sys_constants
-- ----------------------------
BEGIN;
INSERT INTO `sys_constants` VALUES ('1181208349362913282', '系统名称', 'INS1ST_SYSTEM_NAME', 'ins1st-cloud v1.0 | 很赞的后台管理系统', '默认系统名称');
INSERT INTO `sys_constants` VALUES ('1181208349362913283', '码云地址', 'GITEE_URL', 'https://gitee.com/sdjwj1118/ins1st-plus', '码云项目地址');
INSERT INTO `sys_constants` VALUES ('1181208349362913284', '关键词', 'KEYWORDS', 'ins1st-cloud v1.0,基于SpringBoot、SpringCloud、SpringCloudAlibaba分布式开发框架', '关键词');
COMMIT;

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `login_ip` varchar(30) DEFAULT NULL,
  `login_success` char(1) DEFAULT NULL,
  `login_result` varchar(50) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_login_log` VALUES ('1198077485510590465', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-23 03:15:06');
INSERT INTO `sys_login_log` VALUES ('1198097598691426306', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-23 04:35:01');
INSERT INTO `sys_login_log` VALUES ('1198099418264494081', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-23 04:42:15');
INSERT INTO `sys_login_log` VALUES ('1198135009215516673', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-23 07:03:41');
INSERT INTO `sys_login_log` VALUES ('1199194489944641537', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:13:40');
INSERT INTO `sys_login_log` VALUES ('1199194659000258561', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:14:21');
INSERT INTO `sys_login_log` VALUES ('1199194860960190465', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:15:09');
INSERT INTO `sys_login_log` VALUES ('1199194862897958913', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:15:09');
INSERT INTO `sys_login_log` VALUES ('1199195045299851265', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:15:53');
INSERT INTO `sys_login_log` VALUES ('1199195445142851585', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:17:28');
INSERT INTO `sys_login_log` VALUES ('1199197086927654914', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:24:00');
INSERT INTO `sys_login_log` VALUES ('1199197230125387777', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 05:24:34');
INSERT INTO `sys_login_log` VALUES ('1199209450066436098', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:13:07');
INSERT INTO `sys_login_log` VALUES ('1199209579083227138', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:13:38');
INSERT INTO `sys_login_log` VALUES ('1199210260473077762', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:16:20');
INSERT INTO `sys_login_log` VALUES ('1199210505248464897', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:17:19');
INSERT INTO `sys_login_log` VALUES ('1199211839729197057', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:22:37');
INSERT INTO `sys_login_log` VALUES ('1199211909547581441', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:22:54');
INSERT INTO `sys_login_log` VALUES ('1199211945178193921', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:23:02');
INSERT INTO `sys_login_log` VALUES ('1199212774626951170', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 06:26:20');
INSERT INTO `sys_login_log` VALUES ('1199227212096696321', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 07:23:42');
INSERT INTO `sys_login_log` VALUES ('1199235593020641281', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 07:57:00');
INSERT INTO `sys_login_log` VALUES ('1199235929416421378', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 07:58:20');
INSERT INTO `sys_login_log` VALUES ('1199238943388856322', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:10:19');
INSERT INTO `sys_login_log` VALUES ('1199241676409909249', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:21:11');
INSERT INTO `sys_login_log` VALUES ('1199246603018403842', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:40:45');
INSERT INTO `sys_login_log` VALUES ('1199247473898541057', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:44:13');
INSERT INTO `sys_login_log` VALUES ('1199247960387473409', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:46:09');
INSERT INTO `sys_login_log` VALUES ('1199248195310440449', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 08:47:05');
INSERT INTO `sys_login_log` VALUES ('1199262886631849986', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:45:28');
INSERT INTO `sys_login_log` VALUES ('1199264068980994050', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:50:09');
INSERT INTO `sys_login_log` VALUES ('1199264072697147394', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:50:10');
INSERT INTO `sys_login_log` VALUES ('1199264532569022466', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:52:00');
INSERT INTO `sys_login_log` VALUES ('1199264633597222913', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:52:24');
INSERT INTO `sys_login_log` VALUES ('1199265956220702721', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:57:39');
INSERT INTO `sys_login_log` VALUES ('1199266180095873026', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 09:58:33');
INSERT INTO `sys_login_log` VALUES ('1199267552514031617', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 10:04:00');
INSERT INTO `sys_login_log` VALUES ('1199289658324910082', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 11:31:50');
INSERT INTO `sys_login_log` VALUES ('1199289715627491329', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 11:32:04');
INSERT INTO `sys_login_log` VALUES ('1199313951905656833', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-26 13:08:22');
INSERT INTO `sys_login_log` VALUES ('1199526939543584769', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-27 03:14:43');
INSERT INTO `sys_login_log` VALUES ('1199530564797894657', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-27 03:29:07');
INSERT INTO `sys_login_log` VALUES ('1199867809799110657', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 01:49:12');
INSERT INTO `sys_login_log` VALUES ('1199868036203446273', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 01:50:06');
INSERT INTO `sys_login_log` VALUES ('1199873389125206018', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 02:11:23');
INSERT INTO `sys_login_log` VALUES ('1199873980178137089', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 02:13:44');
INSERT INTO `sys_login_log` VALUES ('1199874063489597441', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 02:14:03');
INSERT INTO `sys_login_log` VALUES ('1199948148466814978', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-28 07:08:27');
INSERT INTO `sys_login_log` VALUES ('1201330495078576129', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 02:41:24');
INSERT INTO `sys_login_log` VALUES ('1201332310302044162', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 02:48:37');
INSERT INTO `sys_login_log` VALUES ('1201334372309970945', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 02:56:48');
INSERT INTO `sys_login_log` VALUES ('1201376827814256641', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 05:45:31');
INSERT INTO `sys_login_log` VALUES ('1201378277780922370', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 05:51:17');
INSERT INTO `sys_login_log` VALUES ('1201378678420881409', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 05:52:52');
INSERT INTO `sys_login_log` VALUES ('1201379035913945089', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 05:54:17');
INSERT INTO `sys_login_log` VALUES ('1201381236443013121', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 06:03:02');
INSERT INTO `sys_login_log` VALUES ('1201382737731219458', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 06:09:00');
INSERT INTO `sys_login_log` VALUES ('1201382942652329985', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-02 06:09:49');
INSERT INTO `sys_login_log` VALUES ('1201691120103432194', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-03 02:34:24');
INSERT INTO `sys_login_log` VALUES ('1202038995928743938', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-04 01:36:44');
INSERT INTO `sys_login_log` VALUES ('1202039330332246018', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-04 01:38:04');
INSERT INTO `sys_login_log` VALUES ('1202098114194870273', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-04 05:31:39');
INSERT INTO `sys_login_log` VALUES ('1202098888274677762', 'test', '0:0:0:0:0:0:0:1', '2', '密码错误', '2019-12-04 05:34:44');
INSERT INTO `sys_login_log` VALUES ('1202098911108468738', 'test', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-12-04 05:34:49');
INSERT INTO `sys_login_log` VALUES ('1202100394516312065', 'test', '0:0:0:0:0:0:0:1', '2', '该账号无角色权限', '2019-12-04 05:40:43');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `p_id` varchar(64) DEFAULT NULL COMMENT '父级菜单',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单地址',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单权限',
  `is_menu` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是不是菜单',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标(只限一级菜单使用)',
  `sort` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', '#', NULL, '0', '&#xe6b8;', '6');
INSERT INTO `sys_menu` VALUES ('10', '9', '新增', NULL, 'sys:menu:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('11', '9', '编辑', NULL, 'sys:menu:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('1196756095591370754', '0', '控制台', '/welcome', NULL, '1', '&#xe654;', '1');
INSERT INTO `sys_menu` VALUES ('1196985209589886977', '0', '监控台', '/console', NULL, '1', '&#xe61b;', '2');
INSERT INTO `sys_menu` VALUES ('1196996016964923394', '2', '分配权限', NULL, 'sys:user:role', '0', '', '6');
INSERT INTO `sys_menu` VALUES ('1197018162537721858', '0', '服务监控', 'http://localhost:8769', '', '1', '&#xe7d4;', '3');
INSERT INTO `sys_menu` VALUES ('1197035850223906818', '0', '日志管理', '', '', '0', '&#xe7cb;', '7');
INSERT INTO `sys_menu` VALUES ('1197037164202881025', '1197035850223906818', '登入日志', '/log/login/index', '', '1', NULL, '1');
INSERT INTO `sys_menu` VALUES ('1197037312211480578', '1197035850223906818', '业务日志', '/log/service/index', '', '1', NULL, '2');
INSERT INTO `sys_menu` VALUES ('1198071522082689025', '0', '租户管理', '/tenant/index', '', '1', '&#xe6ca;', '5');
INSERT INTO `sys_menu` VALUES ('12', '9', '删除', NULL, 'sys:menu:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('1202039861528264706', '0', '代码生成', '/gen/index', '', '1', '&#xe73a;', '4');
INSERT INTO `sys_menu` VALUES ('13', '4', '新增', NULL, 'sys:role:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('14', '4', '编辑', NULL, 'sys:role:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('15', '4', '删除', NULL, 'sys:role:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('16', '4', '分配菜单', NULL, 'sys:role:menu', '0', NULL, '4');
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', '/sys/user/index', NULL, '1', NULL, '1');
INSERT INTO `sys_menu` VALUES ('3', '2', '新增', NULL, 'sys:user:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '角色管理', '/sys/role/index', 'sys:role:index', '1', NULL, '4');
INSERT INTO `sys_menu` VALUES ('5', '2', '编辑', NULL, 'sys:user:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('6', '2', '删除', NULL, 'sys:user:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('7', '2', '启用', NULL, 'sys:user:enable', '0', NULL, '4');
INSERT INTO `sys_menu` VALUES ('8', '2', '停用', NULL, 'sys:user:disable', '0', NULL, '5');
INSERT INTO `sys_menu` VALUES ('9', '1', '菜单管理', '/sys/menu/index', 'sys:menu:index', '1', NULL, '2');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` varchar(255) DEFAULT NULL COMMENT '角色状态',
  `keyword` varchar(255) DEFAULT NULL COMMENT '角色标识',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES ('1194858296071180290', '超级管理员', '0', 'admin', '2019-11-14');
INSERT INTO `sys_role` VALUES ('1194911442151718914', '测试角色', '0', 'test', '2019-11-14');
INSERT INTO `sys_role` VALUES ('1194911493758435330', '业务员', '0', 'business', '2019-11-14');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `role_id` varchar(64) DEFAULT NULL,
  `menu_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES ('1196340970904481794', '1194911493758435330', '1');
INSERT INTO `sys_role_menu` VALUES ('1196340970933841922', '1194911493758435330', '2');
INSERT INTO `sys_role_menu` VALUES ('1196340970942230530', '1194911493758435330', '32');
INSERT INTO `sys_role_menu` VALUES ('1202040633519276033', '1194858296071180290', '1');
INSERT INTO `sys_role_menu` VALUES ('1202040633531858946', '1194858296071180290', '2');
INSERT INTO `sys_role_menu` VALUES ('1202040633536053250', '1194858296071180290', '1196996016964923394');
INSERT INTO `sys_role_menu` VALUES ('1202040633548636162', '1194858296071180290', '3');
INSERT INTO `sys_role_menu` VALUES ('1202040633557024769', '1194858296071180290', '5');
INSERT INTO `sys_role_menu` VALUES ('1202040633565413378', '1194858296071180290', '6');
INSERT INTO `sys_role_menu` VALUES ('1202040633573801986', '1194858296071180290', '7');
INSERT INTO `sys_role_menu` VALUES ('1202040633586384897', '1194858296071180290', '8');
INSERT INTO `sys_role_menu` VALUES ('1202040633594773506', '1194858296071180290', '4');
INSERT INTO `sys_role_menu` VALUES ('1202040633607356417', '1194858296071180290', '13');
INSERT INTO `sys_role_menu` VALUES ('1202040633611550722', '1194858296071180290', '14');
INSERT INTO `sys_role_menu` VALUES ('1202040633624133633', '1194858296071180290', '15');
INSERT INTO `sys_role_menu` VALUES ('1202040633632522241', '1194858296071180290', '16');
INSERT INTO `sys_role_menu` VALUES ('1202040633636716546', '1194858296071180290', '9');
INSERT INTO `sys_role_menu` VALUES ('1202040633649299457', '1194858296071180290', '10');
INSERT INTO `sys_role_menu` VALUES ('1202040633653493761', '1194858296071180290', '11');
INSERT INTO `sys_role_menu` VALUES ('1202040633661882370', '1194858296071180290', '12');
INSERT INTO `sys_role_menu` VALUES ('1202040633670270977', '1194858296071180290', '1196756095591370754');
INSERT INTO `sys_role_menu` VALUES ('1202040633674465282', '1194858296071180290', '1196985209589886977');
INSERT INTO `sys_role_menu` VALUES ('1202040633678659585', '1194858296071180290', '1197018162537721858');
INSERT INTO `sys_role_menu` VALUES ('1202040633687048194', '1194858296071180290', '1197035850223906818');
INSERT INTO `sys_role_menu` VALUES ('1202040633691242497', '1194858296071180290', '1197037164202881025');
INSERT INTO `sys_role_menu` VALUES ('1202040633699631106', '1194858296071180290', '1197037312211480578');
INSERT INTO `sys_role_menu` VALUES ('1202040633703825410', '1194858296071180290', '1198071522082689025');
INSERT INTO `sys_role_menu` VALUES ('1202040633712214017', '1194858296071180290', '1202039861528264706');
COMMIT;

-- ----------------------------
-- Table structure for sys_service_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_service_log`;
CREATE TABLE `sys_service_log` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `log_name` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `method_params` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sys_tenants
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenants`;
CREATE TABLE `sys_tenants` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tenant_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tenant_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_tenants
-- ----------------------------
BEGIN;
INSERT INTO `sys_tenants` VALUES ('1197864238928953345', '1197864238920564737', '默认租户', '0', '2019-11-22 13:07:44');
COMMIT;

-- ----------------------------
-- Table structure for sys_upload
-- ----------------------------
DROP TABLE IF EXISTS `sys_upload`;
CREATE TABLE `sys_upload` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `upload_type` char(1) DEFAULT NULL,
  `file_key` varchar(255) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(64) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登入账号',
  `user_nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户密码',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户邮箱',
  `user_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户电话',
  `user_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户头像',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户性别',
  `login_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否登入 0 可登入 1 不可登入',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES ('1194579339778211841', 'admin', '爸爸', '32ff9ee7e841b26a966870c144fdcaec', '42@qq.com', '17717961296', 'http://q1azoor35.bkt.clouddn.com/e0c81223ce714907805c6fecd40c654e', '1', '0', '2019-11-13 11:34:43');
INSERT INTO `sys_user` VALUES ('1202098229286572033', 'test', 'test', 'b087f5af770098d71bd00672b4cfb3bd', '4@qq.com', '1383838438', NULL, '1', '0', '2019-12-04 05:32:06');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `user_id` varchar(64) DEFAULT NULL,
  `role_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES ('1194977980250705922', '1194579339778211841', '1194858296071180290');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
