package com.ins1st.entity;

/**
 * @program: ins1st-single
 * @description:
 * @author: coderSun
 * @create: 2019-12-13 10:39
 **/
public class GenEntity {

    private String author;
    private String modularName;
    private String tableName;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getModularName() {
        return modularName;
    }

    public void setModularName(String modularName) {
        this.modularName = modularName;
    }

    public String getTableName() {
        String[] arr = tableName.split("-");
        return arr[0];
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
