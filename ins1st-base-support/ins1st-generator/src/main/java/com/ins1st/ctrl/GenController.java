package com.ins1st.ctrl;

import com.ins1st.base.R;
import com.ins1st.entity.GenEntity;
import com.ins1st.service.GenService;
import com.ins1st.util.DatabaseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-single
 * @description:
 * @author: coderSun
 * @create: 2019-12-13 10:14
 **/
@RequestMapping(value = "/gen")
@Controller
public class GenController {

    private static final String MODEL = "pages/sys/gen/";

    @Autowired
    private DatabaseUtil databaseUtil;
    @Autowired
    private GenService genService;

    @RequestMapping(value = "index")
    public String index(Model model) {
        model.addAttribute("tables", databaseUtil.getTableNamesWithComment());
        return MODEL + "index.html";
    }


    @RequestMapping(value = "/start")
    @ResponseBody
    public Object start(GenEntity genEntity) {
        genService.start(genEntity);
        return R.success();
    }
}
