package com.ins1st.base;

import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-11 11:10
 **/
public class R<T> {

    private int code;
    private String message;
    private T data;
    private boolean success;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static PageResult page(IPage page) {
        PageResult result = new PageResult();
        result.setCount(page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    public static R success(String message) {
        R r = new R();
        r.setCode(200);
        r.setSuccess(true);
        r.setMessage(message);
        return r;
    }

    public static R success() {
        R r = new R();
        r.setCode(200);
        r.setSuccess(true);
        r.setMessage("请求成功");
        return r;
    }

    public static R success(Object data) {
        R r = new R();
        r.setCode(200);
        r.setSuccess(true);
        r.setMessage("请求成功");
        r.setData(data);
        return r;
    }

    public static R fail(String message) {
        R r = new R();
        r.setCode(200);
        r.setSuccess(false);
        r.setMessage(message);
        return r;
    }

    public static R fail(int code, String message) {
        R r = new R();
        r.setCode(code);
        r.setSuccess(false);
        r.setMessage(message);
        return r;
    }

    public static R fail() {
        R r = new R();
        r.setCode(200);
        r.setSuccess(false);
        r.setMessage("请求失败");
        return r;
    }


}
