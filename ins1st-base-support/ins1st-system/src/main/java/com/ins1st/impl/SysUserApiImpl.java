package com.ins1st.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysUserApi;
import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysUser;
import com.ins1st.mapper.SysUserMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-08 11:23
 **/
@Transactional
@Service
public class SysUserApiImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserApi {


    @Override
    public PageResult page(SysUser sysUser) {
        QueryWrapper qw = new QueryWrapper();
        qw.like(StringUtils.isNotBlank(sysUser.getUserName()), "user_name", sysUser.getUserName());
        qw.like(StringUtils.isNotBlank(sysUser.getUserMobile()), "user_mobile", sysUser.getUserMobile());
        IPage<SysUser> page = this.baseMapper.selectPage(new Page(sysUser.getPage(), sysUser.getLimit()), qw);
        return R.page(page);
    }

    @Override
    public R insertOrUpdate(SysUser sysUser) {
        if (StringUtils.isNotBlank(sysUser.getId())) {
            this.baseMapper.updateById(sysUser);
        } else {
            sysUser.setCreateTime(new Date());
            sysUser.setUserPassword(SecureUtil.md5(sysUser.getUserName() + "-" + sysUser.getUserPassword()));
            this.baseMapper.insert(sysUser);
        }
        return R.success("操作成功");
    }

    @Override
    public R remove(String id) {
        this.baseMapper.deleteById(id);
        return R.success("操作成功");
    }

    @Override
    public R<SysUser> selectOne(SysUser sysUser) {
        QueryWrapper<SysUser> qw = new QueryWrapper<>();
        qw.eq(StringUtils.isNotBlank(sysUser.getId()), "id", sysUser.getId());
        qw.eq(StringUtils.isNotBlank(sysUser.getUserName()), "user_name", sysUser.getUserName());
        qw.eq(StringUtils.isNotBlank(sysUser.getUserPassword()), "user_password", sysUser.getUserPassword());
        SysUser result = this.baseMapper.selectOne(qw);
        return R.success(result);
    }

}
