package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysUserRole;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-14 17:21
 **/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
