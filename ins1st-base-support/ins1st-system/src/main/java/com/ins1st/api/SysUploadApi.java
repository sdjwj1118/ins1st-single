package com.ins1st.api;

import com.ins1st.base.R;
import com.ins1st.entity.SysUpload;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-21 15:49
 **/
public interface SysUploadApi {

    R<SysUpload> insert(SysUpload sysUpload);
}
