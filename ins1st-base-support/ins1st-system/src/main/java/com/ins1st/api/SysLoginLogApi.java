package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysLoginLog;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 13:49
 **/
public interface SysLoginLogApi {

    PageResult page(SysLoginLog sysLoginLog);

    R insert(SysLoginLog sysLoginLog);
}
