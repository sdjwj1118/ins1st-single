package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysUser;

public interface SysUserApi {


    PageResult page(SysUser sysUser);

    R insertOrUpdate(SysUser sysUser);

    R remove(String id);

    R<SysUser> selectOne(SysUser sysUser);
}
