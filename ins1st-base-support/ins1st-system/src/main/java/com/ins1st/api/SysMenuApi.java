package com.ins1st.api;

import com.ins1st.base.R;
import com.ins1st.entity.SysMenu;
import com.ins1st.tree.Ztree;

import java.util.List;

public interface SysMenuApi {

    R queryIndexMenus(String userId);

    R<List<Ztree>> selectZtree(String roleId);

    R saveRoleMenus(String roleId, String menuIds);

    R<List<SysMenu>> list(SysMenu sysMenu);

    R del(String id);

    R<SysMenu> selectOne(String id);

    R insertOrUpdate(SysMenu sysMenu);

    R<List<SysMenu>> queryMenusByUserId(String id);

}
