package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysRole;
import com.ins1st.tree.Ztree;

import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-14 13:09
 **/
public interface SysRoleApi {

    PageResult page(SysRole sysRole);

    R insertOrUpdate(SysRole sysRole);

    R remove(String id);

    R<SysRole> selectOne(String id);

    R<List<Ztree>> selectZtree(String userId);

    R saveUserRoles(String userId, String roleIds);
}
