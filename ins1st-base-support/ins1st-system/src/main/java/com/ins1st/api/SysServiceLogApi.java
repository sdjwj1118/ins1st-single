package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysServiceLog;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 16:17
 **/
public interface SysServiceLogApi {

    PageResult page(SysServiceLog sysServiceLog);

    R insert(SysServiceLog sysServiceLog);
}
