package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysTenants;

import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-22 14:41
 **/
public interface SysTenantsApi {


    R<List<SysTenants>> queryList(SysTenants sysTenants);

    PageResult page(SysTenants sysTenants);

    R insertOrUpdate(SysTenants sysTenants);

    R remove(String id);

    R<SysTenants> selectOne(SysTenants sysTenants);
}
