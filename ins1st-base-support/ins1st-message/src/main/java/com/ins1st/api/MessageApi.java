package com.ins1st.api;

import com.ins1st.entity.JavaMail;

import java.util.Map;

public interface MessageApi {


    /**
     * 发送邮件
     *
     * @param javaMail
     * @return
     */
    boolean sendEmail(JavaMail javaMail);


    /**
     * 发送短信
     *
     * @param tpId
     * @param mobile
     * @param map
     * @return
     */
    boolean sendSms(String tpId, String mobile, Map<String, String> map);
}
