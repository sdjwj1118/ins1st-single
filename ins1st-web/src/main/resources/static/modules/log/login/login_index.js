layui.use(['table', 'ax', 'form'], function () {
    let table = layui.table;
    let $ax = layui.ax;
    let form = layui.form;

    table.render({
        elem: '#tableId',
        url: ctx + '/log/login/page',
        limit: 20,
        size: "sm",
        page: true,
        toolbar: true,
        toolbar: "#toolbarTpl",
        cols: [[
            {type: "radio", fixed: "left"},
            {field: "id", title: "ID", width: 100, sort: true},
            {field: "loginName", title: "登入账号"},
            {field: "loginIp", title: "登入IP"},
            {
                field: "loginSuccess", title: "登入成功",  templet: function (d) {
                    if(d.loginSuccess == '1'){
                        return "成功"
                    }
                    return "失败";
                }
            },
            {field: "loginResult", title: "登入结果"},
            {field: "loginTime", title: "登入时间"},
        ]]
    });
});