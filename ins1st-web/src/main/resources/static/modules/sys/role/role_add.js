layui.use(['form', 'ax'], function () {
    let form = layui.form;
    let $ax = layui.ax;

    form.on("submit(add)", function (data) {
        var ax = new $ax(ctx + "/sys/role/insertOrUpdate", function (result) {
            admin.success(result.message, function () {
                admin.close();
            });
        });
        ax.postForm(data.field);
        ax.start();
        return false;
    });
});