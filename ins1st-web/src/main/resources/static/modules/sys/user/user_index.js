layui.use(['table', 'ax', 'form'], function () {
    let table = layui.table;
    let $ax = layui.ax;
    let form = layui.form;

    let userTable = table.render({
        elem: '#tableId',
        url: ctx + '/sys/user/page',
        limit: 20,
        size: "sm",
        page: true,
        toolbar: true,
        toolbar: "#toolbarTpl",
        cols: [[
            {type: "radio", fixed: "left"},
            {field: "id", title: "ID", width: 100, sort: true},
            {field: "userName", title: "账号", width: 120},
            {field: "userNick", title: "昵称", width: 120},
            {field: "userEmail", title: "邮箱", width: 120},
            {field: "userMobile", title: "电话", width: 120},
            {
                field: "userSex", title: "性别", width: 80, templet: function (d) {
                    if (d.userSex == '1') {
                        return "男";
                    }
                    return "女";
                }
            },
            {
                field: "loginStatus", title: "状态", width: 80, templet: function (d) {
                    if (d.loginStatus == '0') {
                        return '<span class="layui-btn layui-btn-normal layui-btn-xs">正常</span>';
                    }
                    return '<span class="layui-btn layui-btn-danger layui-btn-xs">禁用</span>';
                }
            },
            {field: "createTime", title: "创建时间", width: 150},
            {title: "操作", align: "center", fixed: "right", templet: "#operationTpl"}
        ]]
    });

    form.on("submit(search)", function () {
        let queryData = {};
        queryData['userName'] = $("#userName").val();
        queryData['userMobile'] = $("#userMobile").val();
        table.reload("tableId", {
            where: queryData, page: {curr: 1}
        });
        return false;
    });

    table.on("tool(tableFilter)", function (obj) {
        switch (obj.event) {
            case "del":
                admin.confirm("是否删除该用户?", function () {
                    var ax = new $ax(ctx + "/sys/user/delete", function (result) {
                        admin.success(result.message);
                        table.reload("tableId");
                    });
                    ax.set("id", obj.data.id);
                    ax.start();
                });
                break;
            case "edit":
                admin.open("编辑用户", ctx + "/sys/user/edit?id=" + obj.data.id, "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
            case "enabled":
                var ax = new $ax(ctx + "/sys/user/update", function (result) {
                    admin.success(result.message);
                    table.reload("tableId");
                });
                ax.set("id", obj.data.id);
                ax.set("loginStatus", "0");
                ax.start();
                break;
            case "disabled":
                var ax = new $ax(ctx + "/sys/user/update", function (result) {
                    admin.success(result.message);
                    table.reload("tableId");
                });
                ax.set("id", obj.data.id);
                ax.set("loginStatus", "1");
                ax.start();
                break;
        }
    });

    let setting = {
        check: {
            enable: true,
            chkStyle: "checkbox",    //复选框
            chkboxType: {
                "Y": "ps",
                "N": "ps"
            }
        },
        data: {
            simpleData: {
                enable: true,//是否采用简单数据模式
                idKey: "id",//树节点ID名称
                pIdKey: "pId",//父节点ID名称
                rootPId: null,//根节点ID
            }
        }
    };

    table.on("toolbar(tableFilter)", function (obj) {
        let checked = table.checkStatus('tableId').data;
        switch (obj.event) {
            case "add":
                admin.open("添加用户", ctx + "/sys/user/add", "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
            case "role":
                if (checked.length == 0) {
                    admin.error("请先选中一条记录");
                    return false;
                }
                var ax = new $ax(ctx + "/sys/role/selectZtree?userId=" + checked[0].id, function (result) {
                    let index = layer.open({
                        type: 1,
                        btn: ["确定", "取消"],
                        yes: function () {
                            let checkedNodes = $.fn.zTree.getZTreeObj("tree").getCheckedNodes();
                            let ids = "";
                            for (let i = 0; i < checkedNodes.length; i++) {
                                ids = ids + checkedNodes[i].id + ",";
                            }
                            var ax2 = new $ax(ctx + "/sys/role/saveUserRoles", function (result) {
                                if (result.success) {
                                    admin.success("分配成功");
                                    layer.close(index);
                                }
                            });
                            ax2.set("userId", checked[0].id);
                            ax2.set("roleIds", ids);
                            ax2.start();
                        },
                        title: "分配角色",
                        area: ['300px', '350px'],
                        content: "<div><ul id='tree' class='ztree'></ul></div>",
                    });
                    $.fn.zTree.init($("#tree"), setting, result.data);
                });
                ax.start();
                break;
        }
    });
});