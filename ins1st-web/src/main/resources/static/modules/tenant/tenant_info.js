layui.use(['form', 'ax'], function () {
    let form = layui.form;
    let $ax = layui.ax;

    form.on("submit(add)", function (data) {
        admin.show();
        var ax = new $ax(ctx + "/tenant/insertOrUpdate", function (result) {
            admin.hide();
            admin.success(result.message, function () {
                admin.close();
            });
        });
        ax.postForm(data.field);
        ax.start();
        return false;
    });
});