layui.use(['table', 'ax', 'form'], function () {
    let table = layui.table;
    let $ax = layui.ax;
    let form = layui.form;

    table.render({
        elem: '#tableId',
        url: ctx + '/tenant/page',
        limit: 20,
        size: "sm",
        page: true,
        toolbar: true,
        toolbar: "#toolbarTpl",
        cols: [[
            {type: "radio", fixed: "left"},
            {field: "id", title: "ID", width: 100, sort: true},
            {field: "tenantName", title: "租户名称", width: 150},
            {field: "tenantId", title: "租户ID", width: 150},
            {
                field: "status", title: "状态", width: 150, templet: function (d) {
                    if (d.status == '0') {
                        return '<span class="layui-btn layui-btn-normal layui-btn-xs">正常</span>';
                    }
                    return '<span class="layui-btn layui-btn-danger layui-btn-xs">禁用</span>';
                }
            },
            {field: "createTime", title: "创建时间", width: 150},
            {title: "操作", align: "center", fixed: "right", templet: "#operationTpl"}
        ]]
    });

    form.on("submit(search)", function () {
        let queryData = {};
        queryData['tenantName'] = $("#tenantName").val();
        table.reload("tableId", {
            where: queryData, page: {curr: 1}
        });
        return false;
    });

    table.on("tool(tableFilter)", function (obj) {
        let data = obj.data;
        switch (obj.event) {
            case "del":
                admin.confirm("确定删除该租户?", function () {
                    var ax = new $ax(ctx + "/tenant/delete", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", data.id);
                    ax.start();
                });
                break;
            case "edit":
                admin.open("修改租户", ctx + "/tenant/info?id=" + obj.data.id, "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
        }
    });

    table.on("toolbar(tableFilter)", function (obj) {
        let checked = table.checkStatus('tableId').data;
        switch (obj.event) {
            case "add" :
                admin.open("添加租户", ctx + "/tenant/info", "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
            case "enabled":
                if (checked.length == 0) {
                    admin.error("请先选中一条数据。");
                    return false;
                }
                admin.confirm("确定启动该租户?", function () {
                    var ax = new $ax(ctx + "/tenant/insertOrUpdate", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", checked[0].id);
                    ax.set("status", "0");
                    ax.start();
                });
                break;
            case "disabled":
                if (checked.length == 0) {
                    admin.error("请先选中一条数据。");
                    return false;
                }
                admin.confirm("确定启动该租户?", function () {
                    var ax = new $ax(ctx + "/tenant/insertOrUpdate", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", checked[0].id);
                    ax.set("status", "1");
                    ax.start();
                });
                break;
        }
    });
});