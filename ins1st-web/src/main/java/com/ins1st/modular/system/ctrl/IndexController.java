package com.ins1st.modular.system.ctrl;

import com.ins1st.api.SysTenantsApi;
import com.ins1st.base.R;
import com.ins1st.entity.SysTenants;
import com.ins1st.entity.SysUser;
import com.ins1st.modular.system.req.SystemHardwareInfo;
import com.ins1st.util.ShiroUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-08 15:53
 **/
@RequestMapping(value = "/")
@Controller
public class IndexController implements ErrorController {

    private static final String MODEL = "pages/";

    private static Logger log = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private SysTenantsApi sysTenantsApi;

    /**
     * 登入
     *
     * @return
     */
    @RequestMapping(value = "/login")
    public String login(Model model) {
        List<SysTenants> sysTenantsList = this.sysTenantsApi.queryList(new SysTenants()).getData();
        model.addAttribute("sysTenantsList", sysTenantsList);
        return MODEL + "login.html";
    }


    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = "/")
    public String index() {
        return MODEL + "index.html";
    }

    /**
     * 介绍页
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/welcome")
    public String welcome(Model model) {
        return MODEL + "welcome.html";
    }

    /**
     * 控制台
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/console")
    public String console(Model model) {
        SystemHardwareInfo systemHardwareInfo = new SystemHardwareInfo();
        systemHardwareInfo.copyTo();
        model.addAttribute("server", systemHardwareInfo);
        return MODEL + "console.html";
    }

    @RequestMapping(value = "/doLogin")
    @ResponseBody
    public Object doLogin(SysUser sysUser, String remember, HttpServletRequest request) {
        UsernamePasswordToken upt = new UsernamePasswordToken(sysUser.getUserName(), sysUser.getUserPassword(), sysUser.getTenantId());
        if (remember.equals("on")) {
            upt.setRememberMe(true);
        }
        try {
            ShiroUtil.getSubject().login(upt);
            ShiroUtil.getSubject().getSession().setAttribute("sessionFlag", true);
        } catch (AuthenticationException e) {
            return R.fail(e.getMessage());
        }
        return R.success("登入成功");
    }

    @RequestMapping(value = "/logout")
    public String logout() {
        ShiroUtil.getSubject().logout();
        return "redirect:/login";
    }

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == 404) {
            return MODEL + "404.html";
        } else if (statusCode == 403) {
            return MODEL + "403.html";
        } else {
            return MODEL + "500.html";
        }

    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
