package com.ins1st;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class Ins1stWebApplication {

    private static final Logger log = LoggerFactory.getLogger(Ins1stWebApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(Ins1stWebApplication.class, args);
        log.info(Ins1stWebApplication.class.getSimpleName() + " is start success");
    }

}
