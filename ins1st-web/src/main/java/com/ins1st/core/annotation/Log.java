package com.ins1st.core.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    String service() default "";

    String value() default "";

    String params() default "";


}
