package com.ins1st.config.shiro;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.context.annotation.Bean;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-19 10:02
 **/
public class ShiroRedisConfig {

    public RedisCacheManager cacheManager(String host, int port, String password) {
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        redisCacheManager.setRedisManager(redisManager(host, port, password));
        return redisCacheManager;
    }


    public RedisManager redisManager(String host, int port, String password) {
        RedisManager redisManager = new RedisManager();
        redisManager.setHost(host + ":" + port);
        redisManager.setPassword(password);
        return redisManager;
    }

    @Bean
    public DefaultWebSessionManager sessionManager(String host, int port, String password) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionDAO(redisSessionDAO(host, port, password));
        return sessionManager;
    }


    @Bean
    public RedisSessionDAO redisSessionDAO(String host, int port, String password) {
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        redisSessionDAO.setKeyPrefix("ins1st::session");
        redisSessionDAO.setRedisManager(redisManager(host, port, password));
        return redisSessionDAO;
    }
}
