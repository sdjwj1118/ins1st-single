package com.ins1st.config.beetl;

import com.ins1st.util.ShiroUtil;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;

/**
 * @program: ins1st-plus
 * @description: beetl拓展配置类
 * @author: coderSun
 * @create: 2019-09-04 14:57
 **/
public class BeetlExtConfig extends BeetlGroupUtilConfiguration {

    @Override
    protected void initOther() {
        groupTemplate.registerFunctionPackage("shiro", new ShiroUtil());
    }
}
